Wouldn't it be nice that you can install Glow master and agents on any computer across internet, and invoke them when you want to?

Glow master and agents are super resource efficient. They would not interfere any normal execution if you do not summon them.

However, one possible drawback is the security. Having one giant cluster over the internet, but no security? It would not work. Luckily, Glow just added 2-way SSL to secure itself.

# How it works
All the communication between Glow master, agents, drivers will be secured by 2-way SSL. All executable should supply SSL certificates.

The SSL certificates can be self-signed. 

Example:

### start master
```
glow master --cert.file=~/ssl/localhost.cer --key.file=~/ssl/localhost.key --ca.file=~/ssl/ca.cer
```
### start one agent

```
./glow agent --dir ~/tmp/1 --port 8931 -host localhost \
  --cert.file=~/ssl/localhost.cer --key.file=~/ssl/localhost.key --ca.file=~/ssl/ca.cer -
```

### start another agent
```
./glow agent --dir ~/tmp/2 --port 8932 --host=localhost \
  --cert.file=~/ssl/localhost.cer --key.file=~/ssl/localhost.key --ca.file=~/ssl/ca.cer 
```

### start any driver program
```
./t -glow -glow.driver.host=localhost \
  -cert.file=~/ssl/localhost.cer -key.file=~/ssl/localhost.key -ca.file=~/ssl/ca.cer 
```


# Generating SSL certificates
Generating SSL certificates needs some patients and discipline. Here are some simple examples that can work.

### Sample openssl.cnf file
```
#/etc/ssl/openssl.cnf

[ req ]
default_md = sha512
distinguished_name = req_distinguished_name
req_extensions = v3_req

[ v3_req ]
subjectAltName = @alt_names

[alt_names]
DNS.1 = server1.example.com
DNS.2 = mail.example.com
DNS.3 = www.example.com
DNS.4 = www.sub.example.com
DNS.5 = mx.example.com
DNS.6 = support.example.com

[ req_distinguished_name ]
countryName = Country
countryName_default = US
countryName_min = 2
countryName_max = 2
localityName = Locality
localityName_default = United States
organizationName = Organization
organizationName_default = Glow
commonName = Common Name
commonName_max = 64

[ certauth ]
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always,issuer:always
basicConstraints = CA:true

[ node ]
basicConstraints = CA:FALSE
keyUsage = digitalSignature, keyEncipherment, dataEncipherment
extendedKeyUsage = serverAuth, clientAuth
nsCertType = server, client

```

## Steps to generate Certificates

```
  # Make a SSL folder first
  mkdir ~/ssl/
  cd ~/ssl/
  # also copy the openssl.cnf to ~/ssl/
  ...
  # Step 1. we need a self signed certificate for our CA.
  openssl req -config ~/ssl/openssl.cnf -newkey rsa:2048 -nodes -keyform PEM -keyout ca.key \
      -x509 -days 3650 -extensions certauth -outform PEM -out ca.cer

  # Step 2. Next we will generate a private SSL key for our server.
  openssl genrsa -out server1.key 2048

  # Step 3. Now generate Certificate Signing Request. For the common name you should put the the server name.
  openssl req -config  ~/ssl/openssl.cnf -new -key server1.key -out server1.req

  # Step 4. With self-signed certificate authority, let's issue server certificate with serial number 100:
  openssl x509 -req -in server1.req -CA ca.cer -CAkey ca.key -set_serial 101 -extfile ~/ssl/openssl.cnf -extensions node -days 3650 -outform PEM -out server1.cer

```

For more certificates, repeat step 2,3,4.