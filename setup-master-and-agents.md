# Setup Master

Here are the help text for running "glow master".

```
$  ./glow help master
usage: glow master [<flags>]

Start a master process

Flags:
  --help            Show context-sensitive help (also try --help-long and --help-man).
  --port=8930       listening port
  --ip="localhost"  listening IP adress

```

Usually you do not need to do anything. Just "./glow master" is fine.

# Setup Agents

Here are help text for running "./glow agent"

```
$  ./glow help agent
usage: glow agent [<flags>]

Channel Agent

Flags:
  --help                     Show context-sensitive help (also try --help-long and --help-man).
  --dir="/var/folders/6g/qx_x6g9n1q15vpcr_19rgzl4syznbm/T/"
                             agent folder to store computed data
  --port=8931                agent listening port
  --master="localhost:8930"  master address
  --dataCenter="defaultDataCenter"
                             data center name
  --rack="defaultRack"       rack name
  --max.executors=8          upper limit of executors
  --cpu.level=1              relative computing power of single cpu core
  --memory=1024              memory size in MB
  --clean.restart            clean up previous dataset files

```

Usually, you would specify "--dir", "--master", "--memory", "--max.executors".

"--dataCenter" and "--rack" should be set if the agents are on different data centers, or on different racks.
